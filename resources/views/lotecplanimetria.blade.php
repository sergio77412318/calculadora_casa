<!--Lote Con Pago de Cesion-->
<link rel="stylesheet" href="{{asset('css/styleforms.css')}}">
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('js/jquery-3.6.1.js')}}"></script>


<div class="hide"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><symbol viewBox="-167.4 267.7 257.7 257.7" id="facebook"><path d="M76.1 267.7h-229.3c-7.9 0-14.2 6.4-14.2 14.2v229.3c0 7.9 6.4 14.2 14.2 14.2h123.4v-99.8h-33.6v-38.9h33.6V358c0-33.3 20.3-51.4 50-51.4 14.2 0 26.5 1.1 30 1.5v34.8H29.6c-16.1 0-19.3 7.7-19.3 18.9v24.8h38.5l-5 38.9H10.4v99.8h65.7c7.9 0 14.2-6.4 14.2-14.2V281.9c0-7.8-6.4-14.2-14.2-14.2z"/></symbol><symbol viewBox="-211.1 354.7 82.6 84.9" id="googleplus"><path d="M-167.5 403.2l-4-3.1c-1.2-1-2.9-2.3-2.9-4.8 0-2.4 1.7-4 3.1-5.4 4.6-3.7 9.3-7.5 9.3-15.7 0-8.4-5.3-12.9-7.8-15h6.8l7.2-4.5h-21.8c-6 0-14.6 1.4-20.9 6.6-4.8 4.1-7.1 9.8-7.1 14.8 0 8.6 6.6 17.4 18.3 17.4 1.1 0 2.3-.1 3.5-.2-.5 1.3-1.1 2.4-1.1 4.3 0 3.4 1.8 5.5 3.3 7.5-5 .3-14.3.9-21.1 5.1-6.5 3.9-8.5 9.5-8.5 13.5 0 8.2 7.7 15.8 23.8 15.8 19 0 29.1-10.5 29.1-20.9.1-7.5-4.4-11.3-9.2-15.4zm-14.5-12.7c-9.5 0-13.8-12.3-13.8-19.7 0-2.9.5-5.9 2.4-8.2 1.8-2.2 4.9-3.7 7.7-3.7 9.2 0 13.9 12.4 13.9 20.4 0 2-.2 5.5-2.8 8.1-1.7 1.7-4.6 3.1-7.4 3.1zm.1 44.5c-11.8 0-19.5-5.7-19.5-13.5 0-7.9 7.1-10.5 9.5-11.4 4.6-1.6 10.6-1.8 11.6-1.8 1.1 0 1.7 0 2.5.1 8.4 6 12.1 9 12.1 14.6.1 6.9-5.6 12-16.2 12zm42.3-44.7v-11.1h-5.5v11.1h-11v5.5h11V407h5.5v-11.2h11.1v-5.5"/></symbol><symbol viewBox="62.4 152.4 487.2 487.2" id="instagram"><path d="M493.4 358.5H451c3.1 12 4.9 24.5 4.9 37.5 0 82.8-67.1 149.9-149.9 149.9S156.1 478.8 156.1 396c0-13 1.8-25.5 4.9-37.5h-42.4v206.1c0 10.3 8.4 18.7 18.7 18.7h337.3c10.3 0 18.8-8.4 18.8-18.7V358.5zm0-131.2c0-10.3-8.4-18.7-18.8-18.7h-56.2c-10.3 0-18.7 8.4-18.7 18.7v56.2c0 10.3 8.4 18.7 18.7 18.7h56.2c10.3 0 18.8-8.4 18.8-18.7v-56.2zm-187.4 75c-51.7 0-93.7 41.9-93.7 93.7 0 51.7 41.9 93.7 93.7 93.7 51.7 0 93.7-42 93.7-93.7 0-51.8-42-93.7-93.7-93.7m187.4 337.3H118.6c-31 0-56.2-25.2-56.2-56.2V208.6c0-31 25.2-56.2 56.2-56.2h374.8c31 0 56.2 25.2 56.2 56.2v374.7c0 31.1-25.2 56.3-56.2 56.3"/></symbol><symbol viewBox="-313 288.4 36.1 37.3" id="paper-airplane"><path d="M-312.1 309l8.8 4.6 1.8 10.7c.1.7.7 1.2 1.3 1.3h.3c.6 0 1.1-.3 1.4-.7l4.6-7.5 9.7 4.8c.2.1.5.2.7.2s.5-.1.7-.2c.5-.2.8-.7.9-1.2l4.8-30c.2-.9-.3-1.9-1.2-2.3-.7-.4-1.6-.4-2.2 0l-31.9 17.4c-.5.3-.8.8-.8 1.5.2.6.5 1.2 1.1 1.4zm27.4 9.3l-7.4-3.6 10.8-17.4-3.4 21zm-11.2-3.9l-3.1 5-1.1-6.3 12.9-12.8-8.7 14.1zm7.8-17.7l-14.1 13.8-5.8-3 19.9-10.8z"/></symbol><symbol viewBox="-326.8 274.8 63.7 64.5" id="person"><path d="M-282.1 306.7l-2.5-1.1 1.8-2.1c2.6-3.1 4.1-6.9 4.1-10.9v-1.2c0-9.2-7.5-16.8-16.8-16.8h-1.2c-9.2 0-16.8 7.6-16.8 16.8v1.2c0 4.2 1.6 8.3 4.6 11.5l1.9 2-2.5 1.2c-10.7 5.1-17.6 16.1-17.6 27.9 0 2.2 1.8 4.1 4.1 4.1s4.1-1.8 4.1-4.1c0-12.5 10.2-22.8 22.8-22.8.2 0 .4 0 .6-.1l.3-.1.3.1c.3 0 .5.1.6.1 12.5 0 22.8 10.2 22.8 22.8 0 2.2 1.8 4.1 4.1 4.1 2.2 0 4.1-1.8 4.1-4.1.1-12.5-7.3-23.6-18.8-28.5zm-4.7-14.4v.4c0 4.7-3.9 8.6-8.6 8.7h-.8c-4.8 0-8.6-3.9-8.6-8.7v-1.2c0-2.3.9-4.5 2.5-6.1 1.6-1.6 3.8-2.5 6.1-2.5h1.2c4.8 0 8.6 3.9 8.6 8.7v.6l-.4.1z"/></symbol><symbol viewBox="-232.1 369.1 41.9 54.2" id="pinterest"><path d="M-209.8 369.1c-14.8 0-22.2 10.6-22.2 19.4 0 5.4 2 10.1 6.4 11.9.7.3 1.4 0 1.6-.8.1-.5.5-1.9.6-2.5.2-.8.1-1.1-.4-1.7-1.3-1.5-2.1-3.4-2.1-6.1 0-7.9 5.9-14.9 15.3-14.9 8.4 0 12.9 5.1 12.9 11.9 0 9-4 16.5-9.9 16.5-3.3 0-5.7-2.7-4.9-6 .9-3.9 2.7-8.2 2.7-11 0-2.5-1.4-4.7-4.2-4.7-3.3 0-6 3.4-6 8.1 0 2.9 1 4.9 1 4.9l-4 17c-1.2 5-.2 11.2-.1 11.8 0 .4.5.5.7.2.3-.4 4.3-5.3 5.6-10.2.4-1.4 2.2-8.6 2.2-8.6 1.1 2.1 4.2 3.9 7.6 3.9 10 0 16.8-9.1 16.8-21.3.1-9.2-7.8-17.8-19.6-17.8z"/></symbol><symbol viewBox="-323.2 278.2 56.3 57.6" id="share"><path d="M-276.9 315.6c-2.6 0-5 1-6.8 2.6l-19.3-9.9c.1-.5.1-1 .1-1.5v-.9l19.5-9.9c1.8 1.5 4 2.4 6.5 2.4 5.6 0 10.1-4.5 10.1-10.1s-4.5-10.1-10.1-10.1-10.1 4.5-10.1 10.1v.9l-19.5 9.9c-1.8-1.5-4-2.4-6.6-2.4-5.6 0-10.1 4.5-10.1 10.1s4.5 10.1 10.1 10.1c2.3 0 4.3-.7 6-2l20 10.2v.5c0 5.6 4.5 10.1 10.1 10.1s10.1-4.5 10.1-10.1c.1-5.5-4.5-10-10-10z"/></symbol><symbol viewBox="0 137.8 612 516.4" id="speech-bubble"><path d="M549.7 199.1v324.2h-63.4L415.6 594l-66.5-70.7H61.3V199.1h488.4m0-61.3H61.3C27 137.8 0 165.9 0 199.1v324.2c0 34.3 28.1 61.3 61.3 61.3h261.8l48.8 50.9c11.4 12.5 27 18.7 43.6 18.7h1c16.6 0 32.2-6.2 43.6-17.7l52-52h38.4c34.3 0 61.3-28.1 61.3-61.3V199.1c-.8-34.3-27.9-61.3-62.1-61.3z"/></symbol><symbol viewBox="9.7 189.1 592.5 413.7" id="twitter"><path d="M239.2 602.2C129 596.7 59.9 550.2 9.7 484.7c26.3 22.6 63.6 42.8 112.6 40.4s82.6-22.6 107.1-47.7c-17.1 2.4-33-4.3-39.8-14.1-9.8-15.9 5.5-30.6 19.6-36.1-36.1 1.8-59.4-13.5-72.2-36.7 8.6-10.4 22-15.9 39.8-16.5-32.4-9.2-60-24.5-65.5-60.6 11.6-1.8 21.4-4.9 34.9-4.3-23.9-15.3-50.8-34.9-49.6-75.3 36.1 13.5 72.8 28.2 106.5 44.7 34.3 16.5 67.3 33 91.2 60 21.4-56.9 45.9-113.8 93-144.4-.6 7.3-4.3 11.6-7.3 15.9 12.2-9.8 26.3-17.7 45.3-20.8-3.1 11-11.6 15.9-21.4 19.6 6.7-1.8 14.1-4.9 23.3-7.3 6.1-1.8 25.1-6.7 25.1 3.1 0 8.6-14.1 12.2-22 14.7-11.6 3.7-20.2 4.9-28.2 8.6 41.6-1.2 69.8 17.7 90.6 39.2 19 19.6 31.8 45.3 37.9 74.7 22 8 53.2 1.2 69.8-7.3-9.2 22.6-32.4 36.1-63.6 38.6 16.5 7.3 42.2 9.8 65.5 7.3.6 1.2-2.4 4.3-4.3 6.1-14.8 12.6-36.9 20.5-65 20.5-16.5 61.2-53.9 106.5-99.8 138.3-46.5 32.4-105.9 54.5-181.2 57.5-3.6-.6-8.5-.6-12.8-.6z"/></symbol><symbol viewBox="-324.7 285.2 59.4 43.7" id="video-player"><path d="M-271.2 285.2h-47.6c-3.3 0-5.9 2.7-5.9 5.9v31.8c0 3.3 2.7 5.9 5.9 5.9h47.6c3.3 0 5.9-2.7 5.9-5.9v-31.8c0-3.3-2.6-5.9-5.9-5.9zm0 37.7h-47.6v-31.8h47.6v31.8zM-304.2 319.3c.5.3 1 .4 1.5.4s1-.1 1.5-.4l16.8-9.7c.9-.5 1.5-1.5 1.5-2.6s-.6-2-1.5-2.6l-16.8-9.7c-.9-.5-2-.5-3 0-.9.5-1.5 1.5-1.5 2.6v19.5c.1 1 .6 2 1.5 2.5zm4.5-16.9l8 4.6-8 4.6v-9.2z"/></symbol></svg></div>

<header class="main-head">
      <nav class="head-nav">
        <ul class="menu">
          <li>
          
            <a href="{{route('lotecpcesion')}}">
              <svg class="person">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#person"></use>
              </svg><span>Lotes Individuales con pago de cesion</span></a>
          </li>
          <li>
            <a href="{{route('lotespcesion')}}">
              <svg class="video-player">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#video-player"></use>
              </svg><span>Lotes Individuales sin pago de cesion</span></a>
          </li>
          <li>
            <a href="{{route('lotecplanimetria')}}">
              <svg class="speech-bubble">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#speech-bubble"></use>
              </svg><span>Lotes Individuales con planimetrias</span></a>
          </li>
          <li>
            <a href="{{route('lotesgrupales')}}">
              <svg class="paper-airplane">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#paper-airplane"></use>
              </svg><span>Lotes Grupales</span></a>
          </li>
        </ul>
      </nav>
</header> <!-- //main-head -->

<div class="wrap-all-the-things">
  <div class="card-header bg-rojo p-4 pt-3 pb-3 text-center">
    <h4>{!! __('Calculadora Mi Casa Segura ') !!}</h4>
  </div>
  <h5 class="text-bold text-center">Datos Del Calculo <br> <small class="pt-3 text-danger">(*)</small> Campos obligatorios</h5>
  <br>
  <form name="miform" id="miform">
    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
    @csrf
    {{--Datos--}}
    <div id="datos_apoderado" class="row fieldGroup"> 
      {{--Sub Alcaldia select--}}
        <div class="row mt-0">
          <div class="col-md-3 col-sm-6 col-xl-3">
              <label><span class="text-danger">(*)</span>Sub Alcaldia
              </label>
              </br>
            </div>
            <div class="col-md-9 col-sm-9">
              <div class="form-group mb-2">
                <select class="form-control text-uppercase " id="subalcaldia_select" name="subalcaldia_select" data-html="true" data-placement="top" >
                  
                </select>
                </br>
              </div>
            </div>
        </div> 
      {{--Distrito select--}}
        <div class="row mt-0">
          <div class="col-md-3 col-sm-6 col-xl-3">
              <label><span class="text-danger">(*)</span>Distrito
              </label>
              </br>
            </div>
            <div class="col-md-9 col-sm-9">
              <div class="form-group mb-2">
                <select class="form-control text-uppercase " id="distritoselect" name="distritoselect" data-html="true" data-placement="top" >
                  
                </select>
                </br>
              </div>
            </div>
        </div> 
      {{--subdistrito select--}}
        <div class="row mt-0">
          <div class="col-md-3 col-sm-6 col-xl-3">
              <label><span class="text-danger">(*)</span>Subdistrito
              </label>
              </br>
            </div>
            <div class="col-md-9 col-sm-9">
              <div class="form-group mb-2">
                <select  class="form-control text-uppercase "  id="subdistrito_select" name="subdistrito_select" data-html="true" data-placement="top" >
          
                </select>
                </br>
              </div>
            </div>
        </div>
      {{--Zona Select--}}
        <div class="row mt-0">
          <div class="col-md-3 col-sm-6 col-xl-3">
              <label><span class="text-danger">(*)</span>Zona
              </label>
              </br>
            </div>
            <div class="col-md-9 col-sm-9">
              <div class="form-group mb-2">     
                  <input type="text" id="zonainputreplace" name="zonainputreplace" class="form-control" placeholder="Zona" value="" readonly>
                </br>
              </div>
            </div>
        </div>
      {{--Tipos de cesiones--}}
        <div class="row mt-0">
          <div class="col-md-3 col-sm-6 col-xl-3">
              <label class="">
                  <span class="text-danger">(*)</span> Tipos de cesiones
              </label>
              </div>
              <div class="col-md-9 col-sm-9">
              <div class="form-group mb-2" id="">
              <select  class="form-control text-uppercase "  id="tipos_cesiones" name="tipos_cesiones" data-html="true" data-placement="top" >
                    
              </select>                
            </div>
          </div>
        </div>
        <h5 class="text-bold text-center">Relacion de Superficies <br></h5>
      {{--Inputs de Relacion de Superficies--}}
      {{--Input Total--}}
        <div class="row mt-0">
          <div class="col-md-3 col-sm-6 col-xl-3">
            <label><span class="text-danger">(*)</span>Sup. Total Util:
            </label>
            </br>
          </div>
          <div class="col-md-9 col-sm-9">
            <div class="form-group mb-2">
              <input type="text" class="form-control" id="superficieminuta" name="superficieminuta" value=""
              onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" data-html="true" data-placement="top" title=""
                placeholder="Sup. Total Util">
              </br>
            </div>
          </div>
        </div>
      {{--Input Legal--}}
        <div class="row mt-0">
          <div class="col-md-3 col-sm-6 col-xl-3">
            <label> <span class="text-danger">(*)</span>Sup. Según Doc Legal:</label>
            </br>
          </div>
          <div class="col-md-9 col-sm-9">
            <div class="form-group mb-2">
              <input type="text" class="form-control" id="superficiemensura" name="superficiemensura"
              onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" data-html="true" data-placement="top"
                placeholder="Sup. Según Doc Legal:">

              </br>
            </div>
          </div>
        </div>
      {{--Input Afectada librada al servicio publico--}}
      <div class="row mt-0">
          <div class="col-md-3 col-sm-6 col-xl-3">
            <label><span class="text-danger">(*)</span>Sup. Afectada librada al servicio publico:</label>
            </br>
          </div>
          <div class="col-md-9 col-sm-9">
            <div class="form-group mb-2">
              <input type="text" class="form-control" id="superficieafectada" name="superficieafectada"
              onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" data-html="true" data-placement="top"
                placeholder="Sup. Afectada librada al servicio publico:">

              </br>
            </div>
          </div>
      </div>
      {{--Input Excedente--}}
      <div class="row mt-0">
          <div class="col-md-3 col-sm-6 col-xl-3">
          <label><span class="text-danger">(*)</span> Sup. Excedente:</label>
            </br>
          </div>
          <div class="col-md-9 col-sm-9">
            <div class="form-group mb-2">
              <input type="text" class="form-control" id="superficieexcedente" name="superficieexcedente"
              onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" data-html="true" data-placement="top"
                placeholder="Sup. Excedente:">
              </br>
            </div>
          </div>
      </div>
      {{--Input no liberada--}}
      <div class="row mt-0">
        <div class="col-md-3 col-sm-6 col-xl-3">
          <label> <span class="text-danger">(*)</span> Sup. no liberada:
            </br>
        </div>
        <div class="col-md-9 col-sm-9">
          <div class="form-group mb-2">
            <input type="text" class="form-control" id="superficietotal" name="superficietotal"
            onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" data-html="true" data-placement="top" title=""
              placeholder="Sup. no liberada:" >
            </br>
          </div>
        </div>
      </div>
      <h5 class="text-bold text-center"> Cesion Efectuada de la planimetria<br></h5>
      <div class="row mt-0">
        <div class="col-md-3 col-sm-6 col-xl-3">
        <label class="">
            <span class="text-danger">(*)</span>% Cesion efectuada
        </label>
        </div>
        <div class="col-md-9 col-sm-9">
        <div class="form-group mb-2">
            <input type="text" class="form-control" id="cesionefectuada" name="cesionefectuada" data-html="true" data-placement="top" title="" placeholder="% Cesion efectuada:" >
           <br>
          </div>
        </div>
      </div>


      <h5 class="text-bold text-center"> AVALUO POR CESIONES NO EFECTUADAS <br></h5>
      <div class="row mt-0">
        <div class="col-md-3 col-sm-6 col-xl-3">
        <label class="">
            <span class="text-danger">(*)</span>Zona Catastral
        </label>
        </div>
        <div class="col-md-9 col-sm-9">
        <div class="form-group mb-2">
            <select class="form-control text-uppercase " id="catastral" name="catastral" data-html="true"
            data-placement="top" title="" required>
            
            </select>
           <br>
          </div>
        </div>
      </div>
      <h5 class="text-bold text-center"> Determinacion del valor catastral </h5> 
      <div  class="row mt-0">
        <div class="col-md-3 col-sm-6 col-xl-3">
        <label class="">
            <span class="text-danger">(*)</span>Material de Via
        </label>
        </div>
        <div class="col-md-9 col-sm-9">
        <div class="form-group mb-2">
            <select class="form-control text-uppercase " id="material_via" name="material_via" data-html="true"data-placement="top" title="" required></select>
          <br>
          </div>
        </div>
      </div>
      <div  class="row mt-0">
        <div class="col-md-3 col-sm-6 col-xl-3">
        <label class="">
            <span class="text-danger">(*)</span>Inclinacion
        </label>
        </div>
        <div class="col-md-9 col-sm-9">
        <div class="form-group ">
            <select class="form-control text-uppercase " id="Inclinacion" name="Inclinacion" data-html="true"
            data-placement="top" title="" required>
            
            </select>
            <!--<input type="text" id="inputhidden11"  name="inputhidden11">-->
            <br>
        </div>
        </div>
      </div>
      <div  class="row mt-0">
        <div class="col-md-3 col-sm-6 col-xl-3">
        <label class="">
            <span class="text-danger">(*)</span>Ubicacion
        </label>
        </div>
        <div class="col-md-9 col-sm-9">
        <div class="form-group mb-2">
            <select class="form-control text-uppercase " id="ubicacion" name="ubicacion" data-html="true"
            data-placement="top" title="" required>         
            </select>
          <br>
        </div>
        </div>
      </div>
      <div  class="row mt-0">
        <div class="col-md-3 col-sm-6 col-xl-3">
            <label class="">
                <span class="text-danger">(*)</span>Servicios
            </label>
            </div>
            <div class="col-md-9 col-sm-9">
            <div class="form-group mb-2" id="check_id_servicios">
                
            </div>
        </div>
          <br>
      </div>
      <br>
      <style>
        .sinborde {
          border: 0;
        }
      </style>
      {{--Tabla de datos auxiliares--}}
      <div><br>
        <table class="table" >
          <thead class="thead-dark">
            <tr class="table-dark">
              <th scope="col">Datos Calculados</th>
              <th scope="col">Valor</th>
              <th scope="col">Unidad</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">% de cesion</th>
              <td><input class="sinborde" id="tdporcentajecesiones" readonly></td>
              <td>%</td>
            </tr>
            <tr>
              <th scope="row">Deficit de Cesion</th>
              <td><input class="sinborde" id="deficitcesion" readonly></td>
              <td>mts</td>
            </tr>
            <tr>
              <th scope="row">Valor Catastral Ponderado</th>
              <td><input class="sinborde" id="tdcatastral" readonly></td>
              <td>mts</td>
            </tr>
            <tr>
              <th scope="row">Factor Material de Via</th>
              <td><input class="sinborde" id="tdmaterialvia" readonly></td>
              <td>Bs</td>
            </tr>
            <tr>
              <th scope="row">Factor Inclinacion</th>
              <td><input class="sinborde" id="tdfactorinclinacion" readonly></td>
              <td></td>
            </tr>
            <tr>
              <th scope="row">Factor Ubicacion Manzano</th>
              <td><input class="sinborde" id="tdfactorubicacion" readonly></td>
              <td></td>
            </tr>
            <tr >
              <th scope="row">Factor Servicios</th>
              <td><input class="sinborde"  id="inputhidden13"  name="inputhidden13" value="0.20" readonly></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
      {{--Boton Para Calcular los valores de la tabla--}}
      <div id="boton_calcular" class="text-center">
        <button type="button" class="btn btn-success" id="btn_calcular" onclick="metodos();" name="btn_calcular">Calcular</button>
        <br></br></br>
      </div>
      {{--Table LOTES INDIVIDUALES CON PAGO DE CESION--}}
      <table class="table table-hover">
              <thead>
                <tr class="table-dark">
                  <th scope="col"></th>
                  <th scope="col">Datos de salida Finales</th>
                  <th scope="col">Valor</th>
                  <th scope="col">Unidad</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th  rowspan="3">Cesiones</th>
                  <th>Cesion no efectuadas</th>
                  <td><input class="sinborde" type="text" id="inputcesionnoefectuada" readonly></td>
                  <th>mts2</th>
                </tr>
                <tr>
                  <th>Valor Catastral</th>
                  <td><input class="sinborde" type="text" id="inputcatastralvalor" readonly></td>
                  <th>Bs/mts2</th>
                </tr>
                <tr>
                  <th>Pago Cesiones</th>
                  <td><input class="sinborde" type="text" id="totalpagocesiones" readonly></td>
                  <th>Bs</th>
                </tr>
                

                <tr>
                  <th  rowspan="3">Tasas de Regularizacion</th>
                  <th>Sup. Total Util</th>
                  <td><input class="sinborde" type="text" id="superficietotalutil" readonly></td>
                  <th>mts2</th>
                </tr>
                <tr>
                  <th>Tasa de Regularizacion</th>
                  <td><input class="sinborde" type="text" id="tasaderegularizacion" value="1" readonly></td>
                  <th>Bs/mts2</th>
                </tr>
                <tr>
                  <th>Pago por Regularizacion</th>
                  <td><input class="sinborde" type="text" id="pagoporregularizacion" readonly></td>
                  <th>Bs</th>
                </tr>


                <tr>
                  <th>Liquidacion Final</th>
                  <th>Monto Liquidacion Total</th>
                  <td><input class="sinborde" type="text" id="montototal" readonly></td>
                  <th>Bs</th>
                </tr>
              
              </tbody>
      </table>





    </div>
  </form>












  
</div>



<!--Consumo de apis de las diferentes tablas de cesiones-->
<script>
  
  var requestOptions = {
  method: 'GET',
  redirect: 'follow'
  };

  fetch("/api/getvalorterreno", requestOptions)
  .then(response => response.json())
  .then(recuperar_terreno => mostrarterreno(recuperar_terreno))
  .catch(error => console.log('error', error));
    const mostrarterreno=(recuperar_terreno)=>{
        let selecterreno=""
        selecterreno=`<option value="">* Seleccione una opción *</option>`
        for(var i=0;i<recuperar_terreno.length;i++){
            selecterreno+=`
        <option value="${recuperar_terreno[i].valor_catastral_gestion_2017}">${recuperar_terreno[i].zona}</option>`;
        }
        document.getElementById('catastral').innerHTML = selecterreno

    }



</script>
<script>
  var requestOptions = {
    method: 'GET',
    redirect: 'follow'
  };

  fetch("/api/getcoeficientedeservicios", requestOptions)
    .then(response => response.json())
    .then(recuperar_servicios => mostrarservicios(recuperar_servicios))
    .catch(error => console.log('error', error));
      const mostrarservicios=(recuperar_servicios)=>{
          let selecservicios=""
          for(var i=0;i<recuperar_servicios.length;i++){
              selecservicios+=`<div class="form-check" >
              <input name="sumcheckid" class="form-check-input mis-checkboxes" type="checkbox" tu-attr-precio="${recuperar_servicios[i].coeficiente}"  id="flexCheckDefault">
                  <label class="form-check-label" for="flexCheckDefault">
                  ${recuperar_servicios[i].descripcion_de_servicios}
                  </label></div>`;
          }
          document.getElementById('check_id_servicios').innerHTML = selecservicios

      }



</script>
<script>
  var requestOptions = {
    method: 'GET',
    redirect: 'follow'
  };

  fetch("/api/getcoeficientedeubicacion", requestOptions)
    .then(response => response.json())
    .then(recuperar_ubicacion => mostrarubicacion(recuperar_ubicacion))
    .catch(error => console.log('error', error));
      const mostrarubicacion=(recuperar_ubicacion)=>{
          let selectubicacion=""
          selectubicacion=`<option value="">* Seleccione una opción *</option>`
          for(var i=0;i<recuperar_ubicacion.length;i++){
              selectubicacion+=`
          <option value="${recuperar_ubicacion[i].coeficiente}">${recuperar_ubicacion[i].factor_de_inclinacion}</option>`;
          }
          document.getElementById('ubicacion').innerHTML = selectubicacion

      }



</script>
<script>
  var requestOptions = {
    method: 'GET',
    redirect: 'follow'
  };

  fetch("/api/getcoeficientetopografico", requestOptions)
    .then(response => response.json())
    .then(recuperar_inclinacion => mostrarinclinacion(recuperar_inclinacion))
    .catch(error => console.log('error', error));
      const mostrarinclinacion=(recuperar_inclinacion)=>{
          let selectinclinacion=""
          selectinclinacion=`<option value="">* Seleccione una opción *</option>`
          for(var i=0;i<recuperar_inclinacion.length;i++){
              selectinclinacion+=`
          <option value="${recuperar_inclinacion[i].coeficiente}">${recuperar_inclinacion[i].factor_de_inclinacion}</option>`;
          }
          document.getElementById('Inclinacion').innerHTML = selectinclinacion

      }



</script>
<script>
  var requestOptions = {
  method: 'GET',
  redirect: 'follow'
  };

  fetch("/api/getcoeficientevia", requestOptions)
  .then(response => response.json())
  .then(recuperar_via => mostrarvia(recuperar_via))
  .catch(error => console.log('error', error));
    const mostrarvia=(recuperar_via)=>{
        let selectvia=""
        selectvia=`<option value="">* Seleccione una opción *</option>`
        for(var i=0;i<recuperar_via.length;i++){
        selectvia+=`
        <option value="${recuperar_via[i].coeficiente}">${recuperar_via[i].material_de_la_via}</option>`;
        }
        document.getElementById('material_via').innerHTML = selectvia

    }



</script>
{{--sub alcaldia--}}
<script>
  var requestOptions = {
    method: 'GET',
    redirect: 'follow'
  };

  fetch("/api/getsubalcaldia", requestOptions)
    .then(response => response.json())
    .then(recuperar_subalcaldia => mostrarsubalcaldia(recuperar_subalcaldia))
    .catch(error => console.log('error', error));
      const mostrarsubalcaldia=(recuperar_subalcaldia)=>{
          let selectsubalcaldia=""
          selectsubalcaldia=`<option value="">* Seleccione una opción *</option>`
          for(var i=0;i<recuperar_subalcaldia.length;i++){
            selectsubalcaldia+=`
            <option value="${recuperar_subalcaldia[i].nombre}">${recuperar_subalcaldia[i].nombre}</option>`;
          }
          document.getElementById('subalcaldia_select').innerHTML = selectsubalcaldia
      }



</script>
{{--Restriccion de distrito--}}
<script>
  var requestOptions = {
    method: 'GET',
    redirect: 'follow'
  };
  fetch("/api/getdistrito", requestOptions)
    .then(response => response.json())
    .then(recuperar_distrito => mostrardistrito(recuperar_distrito))
    .catch(error => console.log('error', error));
      const mostrardistrito=(recuperar_distrito)=>{
     //     console.log(recuperar_distrito);
          let selectdistrito=""
              const tiposubalcaldia =document.querySelector("#subalcaldia_select");
                    tiposubalcaldia.addEventListener("change", () => {
                      selectdistrito=`<option value="">* Seleccione una opción *</option>`
                        //si el select seleccionado es pagos en linea
                        if (tiposubalcaldia.value === "TUNARI") {
                          selectdistrito+=`<option value="${recuperar_distrito[0].numero_distrito}">${recuperar_distrito[0].numero_distrito}</option>
                          <option value="${recuperar_distrito[1].numero_distrito}">${recuperar_distrito[1].numero_distrito}</option>`;
                        }  
                        if (tiposubalcaldia.value === "MOLLE") {
                          selectdistrito+=`<option value="${recuperar_distrito[2].numero_distrito}">${recuperar_distrito[2].numero_distrito}</option>
                          <option value="${recuperar_distrito[3].numero_distrito}">${recuperar_distrito[3].numero_distrito}</option>`;
                        } 
                        if (tiposubalcaldia.value === "ALEJO CALATAYUD") {
                          selectdistrito+=`<option value="${recuperar_distrito[4].numero_distrito}">${recuperar_distrito[4].numero_distrito}</option>
                          <option value="${recuperar_distrito[7].numero_distrito}">${recuperar_distrito[7].numero_distrito}</option>`;
                        }
                        if (tiposubalcaldia.value === "VALLE HERMOSO") {
                          selectdistrito+=`<option value="${recuperar_distrito[5].numero_distrito}">${recuperar_distrito[5].numero_distrito}</option>
                          <option value="${recuperar_distrito[6].numero_distrito}">${recuperar_distrito[6].numero_distrito}</option>
                          <option value="${recuperar_distrito[11].numero_distrito}">${recuperar_distrito[11].numero_distrito}</option>`;
                        }
                        if (tiposubalcaldia.value === "ITOCTA") {
                          selectdistrito+=`<option value="${recuperar_distrito[13].numero_distrito}">${recuperar_distrito[13].numero_distrito}</option>
                          <option value="${recuperar_distrito[12].numero_distrito}">${recuperar_distrito[12].numero_distrito}</option>`;
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO") {
                          selectdistrito+=`<option value="${recuperar_distrito[8].numero_distrito}">${recuperar_distrito[8].numero_distrito}</option>
                          <option value="${recuperar_distrito[9].numero_distrito}">${recuperar_distrito[9].numero_distrito}</option>
                          <option value="${recuperar_distrito[10].numero_distrito}">${recuperar_distrito[10].numero_distrito}</option>`;
                        }  
                        document.getElementById('distritoselect').innerHTML = selectdistrito
              });         
      }

</script>
{{--Restriccion de subdistrito--}}
<script>
  var requestOptions = {
    method: 'GET',
    redirect: 'follow'
  };
  fetch("/api/getsubdistrito", requestOptions)
    .then(response => response.json())
    .then(recuperar_subdistrito => mostrarsubdistrito(recuperar_subdistrito))
    .catch(error => console.log('error', error));
      const mostrarsubdistrito=(recuperar_subdistrito)=>{
       //   console.log(recuperar_subdistrito);
          let selectsubdistrito=""
              const tiposubalcaldia =document.querySelector("#subalcaldia_select");
              const tipodistrito =document.querySelector("#distritoselect");
                    tipodistrito.addEventListener("change", () => {
                      selectsubdistrito=`<option value="">* Seleccione una opción *</option>`
                        //si el select seleccionado es pagos en linea
                        if (tipodistrito.value === "1" && tiposubalcaldia.value==="TUNARI" ) {
                          selectsubdistrito+=`<option value="${recuperar_subdistrito[23].numero_sub_distrito}">${recuperar_subdistrito[23].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[24].numero_sub_distrito}">${recuperar_subdistrito[24].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[25].numero_sub_distrito}">${recuperar_subdistrito[25].numero_sub_distrito}</option>
                          `;
                        }  
                        if (tipodistrito.value === "2" && tiposubalcaldia.value==="TUNARI" ) {
                          selectsubdistrito+=`<option value="${recuperar_subdistrito[0].numero_sub_distrito}">${recuperar_subdistrito[0].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[2].numero_sub_distrito}">${recuperar_subdistrito[2].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[21].numero_sub_distrito}">${recuperar_subdistrito[21].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[22].numero_sub_distrito}">${recuperar_subdistrito[22].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[23].numero_sub_distrito}">${recuperar_subdistrito[23].numero_sub_distrito}</option>`;
                        } 
                        if (tipodistrito.value === "3" && tiposubalcaldia.value==="MOLLE" ) {
                          selectsubdistrito+=`<option value="${recuperar_subdistrito[1].numero_sub_distrito}">${recuperar_subdistrito[1].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[5].numero_sub_distrito}">${recuperar_subdistrito[5].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[20].numero_sub_distrito}">${recuperar_subdistrito[20].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[26].numero_sub_distrito}">${recuperar_subdistrito[26].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[36].numero_sub_distrito}">${recuperar_subdistrito[36].numero_sub_distrito}</option>`;
                        }
                        if (tipodistrito.value === "4" && tiposubalcaldia.value==="MOLLE") {
                          selectsubdistrito+=`<option value="${recuperar_subdistrito[5].numero_sub_distrito}">${recuperar_subdistrito[5].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[9].numero_sub_distrito}">${recuperar_subdistrito[9].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[26].numero_sub_distrito}">${recuperar_subdistrito[26].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[27].numero_sub_distrito}">${recuperar_subdistrito[27].numero_sub_distrito}</option>`;
                        }
                        if (tipodistrito.value === "5" && tiposubalcaldia.value==="ALEJO CALATAYUD") {
                          selectsubdistrito+=`<option value="${recuperar_subdistrito[11].numero_sub_distrito}">${recuperar_subdistrito[11].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[13].numero_sub_distrito}">${recuperar_subdistrito[13].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[14].numero_sub_distrito}">${recuperar_subdistrito[14].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[15].numero_sub_distrito}">${recuperar_subdistrito[15].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[16].numero_sub_distrito}">${recuperar_subdistrito[16].numero_sub_distrito}</option>`;
                        }
                        if (tipodistrito.value === "8" && tiposubalcaldia.value==="ALEJO CALATAYUD") {
                          selectsubdistrito+=`<option value="${recuperar_subdistrito[17].numero_sub_distrito}">${recuperar_subdistrito[17].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[19].numero_sub_distrito}">${recuperar_subdistrito[19].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[33].numero_sub_distrito}">${recuperar_subdistrito[33].numero_sub_distrito}</option>`;
                        }  
                        if (tipodistrito.value === "6" && tiposubalcaldia.value==="VALLE HERMOSO") {
                          selectsubdistrito+=`<option value="${recuperar_subdistrito[15].numero_sub_distrito}">${recuperar_subdistrito[15].numero_sub_distrito}</option>`;

                        }
                        if (tipodistrito.value === "7" && tiposubalcaldia.value==="VALLE HERMOSO") {
                          selectsubdistrito+=`<option value="${recuperar_subdistrito[15].numero_sub_distrito}">${recuperar_subdistrito[15].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[18].numero_sub_distrito}">${recuperar_subdistrito[18].numero_sub_distrito}</option>`;

                        }
                        if (tipodistrito.value === "14" && tiposubalcaldia.value==="VALLE HERMOSO") {
                          selectsubdistrito+=`<option value="${recuperar_subdistrito[18].numero_sub_distrito}">${recuperar_subdistrito[18].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[19].numero_sub_distrito}">${recuperar_subdistrito[19].numero_sub_distrito}</option>`;

                        }
                        if (tipodistrito.value === "9" && tiposubalcaldia.value==="ITOCTA") {
                          selectsubdistrito+=`<option value="${recuperar_subdistrito[13].numero_sub_distrito}">${recuperar_subdistrito[13].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[27].numero_sub_distrito}">${recuperar_subdistrito[27].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[28].numero_sub_distrito}">${recuperar_subdistrito[28].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[29].numero_sub_distrito}">${recuperar_subdistrito[29].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[30].numero_sub_distrito}">${recuperar_subdistrito[30].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[31].numero_sub_distrito}">${recuperar_subdistrito[31].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[34].numero_sub_distrito}">${recuperar_subdistrito[34].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[35].numero_sub_distrito}">${recuperar_subdistrito[35].numero_sub_distrito}</option>`;

                        }
                        if (tipodistrito.value === "15" && tiposubalcaldia.value==="ITOCTA") {
                          selectsubdistrito+=`<option value="${recuperar_subdistrito[31].numero_sub_distrito}">${recuperar_subdistrito[31].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[32].numero_sub_distrito}">${recuperar_subdistrito[32].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[34].numero_sub_distrito}">${recuperar_subdistrito[34].numero_sub_distrito}</option>`;

                        }
                        if (tipodistrito.value === "10" && tiposubalcaldia.value==="ADELA ZAMUDIO") {
                          selectsubdistrito+=`<option value="${recuperar_subdistrito[6].numero_sub_distrito}">${recuperar_subdistrito[6].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[7].numero_sub_distrito}">${recuperar_subdistrito[7].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[10].numero_sub_distrito}">${recuperar_subdistrito[10].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[11].numero_sub_distrito}">${recuperar_subdistrito[11].numero_sub_distrito}</option>`;

                        }
                        if (tipodistrito.value === "11" && tiposubalcaldia.value==="ADELA ZAMUDIO") {
                          selectsubdistrito+=`<option value="${recuperar_subdistrito[8].numero_sub_distrito}">${recuperar_subdistrito[8].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[12].numero_sub_distrito}">${recuperar_subdistrito[12].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[15].numero_sub_distrito}">${recuperar_subdistrito[15].numero_sub_distrito}</option>`;

                        }
                        if (tipodistrito.value === "12" && tiposubalcaldia.value==="ADELA ZAMUDIO") {
                          selectsubdistrito+=`<option value="${recuperar_subdistrito[1].numero_sub_distrito}">${recuperar_subdistrito[1].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[2].numero_sub_distrito}">${recuperar_subdistrito[2].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[3].numero_sub_distrito}">${recuperar_subdistrito[3].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[4].numero_sub_distrito}">${recuperar_subdistrito[4].numero_sub_distrito}</option>
                          <option value="${recuperar_subdistrito[5].numero_sub_distrito}">${recuperar_subdistrito[5].numero_sub_distrito}</option>`;

                        }
                        document.getElementById('subdistrito_select').innerHTML = selectsubdistrito
              });         
      }

</script>
{{--Restriccion de zona--}}
<script>
  var requestOptions = {
    method: 'GET',
    redirect: 'follow'
  };
  fetch("/api/getzona", requestOptions)
    .then(response => response.json())
    .then(recuperar_zona => mostrarzona(recuperar_zona))
    .catch(error => console.log('error', error));
      const mostrarzona=(recuperar_zona)=>{
          let selectzona=""
          let selectzonaaranjuez=""

              const tiposubalcaldia =document.querySelector("#subalcaldia_select");
              const tipodistrito =document.querySelector("#distritoselect");
              const tiposubdistrito =document.querySelector("#subdistrito_select");
              tiposubdistrito.addEventListener("change", () => {
                        
                  //$("#removerinput").remove();

                        if (tiposubalcaldia.value === "TUNARI" && tipodistrito.value==="1" && tiposubdistrito.value==="24") {
                          selectzona=`${recuperar_zona[0].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "TUNARI" && tipodistrito.value==="1" && tiposubdistrito.value==="25") {
                          selectzona =`${recuperar_zona[1].nombre_zona}`;
                        }
                  
                        if (tiposubalcaldia.value === "TUNARI" && tipodistrito.value==="1" && tiposubdistrito.value==="26") {
                          selectzona=`${recuperar_zona[2].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "TUNARI" && tipodistrito.value==="2" && tiposubdistrito.value==="1") {
                          selectzona=`${recuperar_zona[3].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "TUNARI" && tipodistrito.value==="2" && tiposubdistrito.value==="3") {
                          selectzona=`${recuperar_zona[4].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "TUNARI" && tipodistrito.value==="2" && tiposubdistrito.value==="22") {
                          selectzona =`${recuperar_zona[5].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "TUNARI" && tipodistrito.value==="2" && tiposubdistrito.value==="23") {
                          selectzona=`${recuperar_zona[6].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "TUNARI" && tipodistrito.value==="2" && tiposubdistrito.value==="24") {
                          selectzona=`${recuperar_zona[0].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="3" && tiposubdistrito.value==="2") {
                          selectzona=`${recuperar_zona[7].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="3" && tiposubdistrito.value==="6") {
                          selectzona=`${recuperar_zona[8].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="3" && tiposubdistrito.value==="21") {
                          selectzona=`${recuperar_zona[9].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="3" && tiposubdistrito.value==="27") {
                          selectzona=`${recuperar_zona[10].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="3" && tiposubdistrito.value==="37") {
                          selectzona=`${recuperar_zona[11].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="4" && tiposubdistrito.value==="6") {
                          selectzona=`${recuperar_zona[8].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="4" && tiposubdistrito.value==="10") {
                          selectzona=`${recuperar_zona[12].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="4" && tiposubdistrito.value==="27") {
                          selectzona=`${recuperar_zona[10].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="4" && tiposubdistrito.value==="28") {
                          selectzona=`${recuperar_zona[13].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ALEJO CALATAYUD" && tipodistrito.value==="5" && tiposubdistrito.value==="12") {
                          selectzona=`${recuperar_zona[14].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ALEJO CALATAYUD" && tipodistrito.value==="5" && tiposubdistrito.value==="14") {
                          selectzona=`${recuperar_zona[15].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ALEJO CALATAYUD" && tipodistrito.value==="5" && tiposubdistrito.value==="15") {
                          selectzona=`${recuperar_zona[16].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ALEJO CALATAYUD" && tipodistrito.value==="5" && tiposubdistrito.value==="16") {
                          selectzona=`${recuperar_zona[17].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ALEJO CALATAYUD" && tipodistrito.value==="5" && tiposubdistrito.value==="17") {
                          selectzona=`${recuperar_zona[18].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ALEJO CALATAYUD" && tipodistrito.value==="8" && tiposubdistrito.value==="18") {
                          selectzona=`${recuperar_zona[19].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ALEJO CALATAYUD" && tipodistrito.value==="8" && tiposubdistrito.value==="20") {
                          selectzona=`${recuperar_zona[20].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ALEJO CALATAYUD" && tipodistrito.value==="8" && tiposubdistrito.value==="34") {
                          selectzona=`${recuperar_zona[21].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "VALLE HERMOSO" && tipodistrito.value==="6" && tiposubdistrito.value==="16") {
                          selectzona=`${recuperar_zona[17].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "VALLE HERMOSO" && tipodistrito.value==="7" && tiposubdistrito.value==="16") {
                          selectzona=`${recuperar_zona[17].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "VALLE HERMOSO" && tipodistrito.value==="7" && tiposubdistrito.value==="19") {
                          selectzona=`${recuperar_zona[22].nombre_zona}`;
                        }  
                        if (tiposubalcaldia.value === "VALLE HERMOSO" && tipodistrito.value==="14" && tiposubdistrito.value==="19") {
                          selectzona=`${recuperar_zona[22].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "VALLE HERMOSO" && tipodistrito.value==="14" && tiposubdistrito.value==="20") {
                          selectzona=`${recuperar_zona[20].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="9" && tiposubdistrito.value==="14") {
                          selectzona=`${recuperar_zona[15].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="9" && tiposubdistrito.value==="28") {
                          selectzona=`${recuperar_zona[13].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="9" && tiposubdistrito.value==="29") {
                          selectzona=`${recuperar_zona[23].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="9" && tiposubdistrito.value==="30") {
                          selectzona=`${recuperar_zona[24].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="9" && tiposubdistrito.value==="31") {
                          selectzona=`${recuperar_zona[25].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="9" && tiposubdistrito.value==="32") {
                          selectzona=`${recuperar_zona[26].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="9" && tiposubdistrito.value==="35") {
                          selectzona=`${recuperar_zona[27].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="9" && tiposubdistrito.value==="36") {
                          selectzona=`${recuperar_zona[28].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="15" && tiposubdistrito.value==="32") {
                          selectzona=`${recuperar_zona[26].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="15" && tiposubdistrito.value==="33") {
                          selectzona=`${recuperar_zona[29].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="15" && tiposubdistrito.value==="35") {
                          selectzona=`${recuperar_zona[27].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="10" && tiposubdistrito.value==="7") {
                          selectzona=`${recuperar_zona[30].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="10" && tiposubdistrito.value==="8") {
                          selectzona=`${recuperar_zona[31].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="10" && tiposubdistrito.value==="11") {
                          selectzona=`${recuperar_zona[32].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="10" && tiposubdistrito.value==="12") {
                          selectzona=`${recuperar_zona[14].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="11" && tiposubdistrito.value==="9") {
                          selectzona=`${recuperar_zona[33].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="11" && tiposubdistrito.value==="13") {
                          selectzona=`${recuperar_zona[34].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="11" && tiposubdistrito.value==="16") {
                          selectzona=`${recuperar_zona[17].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="12" && tiposubdistrito.value==="2") {
                          selectzona=`${recuperar_zona[7].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="12" && tiposubdistrito.value==="3") {
                          selectzona=`${recuperar_zona[4].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="12" && tiposubdistrito.value==="4") {
                          selectzona=`${recuperar_zona[35].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="12" && tiposubdistrito.value==="5") {
                          selectzona=`${recuperar_zona[36].nombre_zona}`;
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="12" && tiposubdistrito.value==="6") {
                          selectzona=`${recuperar_zona[8].nombre_zona}`;
                        }  
                        document.getElementById('zonainputreplace').value = selectzona;

              });         

      }

</script>
{{--Restriccin tipo de cesiones--}}
<script>
   var requestOptions = {
    method: 'GET',
    redirect: 'follow'
  };

  fetch("/api/gettiposdecesiones", requestOptions)
    .then(response => response.json())
    .then(recuperar_cesiones => mostrarcesiones(recuperar_cesiones))
    .catch(error => console.log('error', error));
      const mostrarcesiones=(recuperar_cesiones)=>{
       // console.log(recuperar_cesiones);
          let selectcesiones=""
          const tiposubalcaldia =document.querySelector("#subalcaldia_select");
              const tipodistrito =document.querySelector("#distritoselect");
              const tiposubdistrito =document.querySelector("#subdistrito_select");
              tiposubdistrito.addEventListener("change", () => {
 
          selectcesiones=`<option value="">* Seleccione una opción *</option>`
          if (tiposubalcaldia.value === "TUNARI" && tipodistrito.value==="1" && tiposubdistrito.value==="24") {                    
            selectcesiones+=` <option value="${recuperar_cesiones[0].cesion_para_urbanizacion}">Cesion Para Urbanizacion</option>
            <option value="${recuperar_cesiones[0].cesion_para_regularizacion}">Cesion Para Regularizacion</option>
            <option value="${recuperar_cesiones[0].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones</option>`;
          }
          if (tiposubalcaldia.value === "TUNARI" && tipodistrito.value==="1" && tiposubdistrito.value==="25") {
            selectcesiones+=` <option value="${recuperar_cesiones[1].cesion_para_urbanizacion}">Cesion Para Urbanizacion</option>
            <option value="${recuperar_cesiones[1].cesion_para_regularizacion}">Cesion Para Regularizacion</option>
            <option value="${recuperar_cesiones[1].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones</option>`;
          }


                        if (tiposubalcaldia.value === "TUNARI" && tipodistrito.value==="1" && tiposubdistrito.value==="26") {
                          selectcesiones+=` <option value="${recuperar_cesiones[1].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[1].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[1].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        }
                        if (tiposubalcaldia.value === "TUNARI" && tipodistrito.value==="2" && tiposubdistrito.value==="1") {
                          selectcesiones+=` <option value="${recuperar_cesiones[0].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[0].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[0].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "TUNARI" && tipodistrito.value==="2" && tiposubdistrito.value==="3") {
                          selectcesiones+=` <option value="${recuperar_cesiones[0].cesion_para_urbanizacion}">Cesion Para Urbanizacion</option>
                                            <option value="${recuperar_cesiones[0].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[0].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "TUNARI" && tipodistrito.value==="2" && tiposubdistrito.value==="22") {
                          selectcesiones+=` <option value="${recuperar_cesiones[1].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[1].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[1].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "TUNARI" && tipodistrito.value==="2" && tiposubdistrito.value==="23") {
                          selectcesiones+=` <option value="${recuperar_cesiones[0].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[0].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[0].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "TUNARI" && tipodistrito.value==="2" && tiposubdistrito.value==="24") {
                          selectcesiones+=` <option value="${recuperar_cesiones[0].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[0].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[0].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="3" && tiposubdistrito.value==="2") {
                          selectcesiones+=` <option value="${recuperar_cesiones[0].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[0].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[0].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="3" && tiposubdistrito.value==="6") {
                          selectcesiones+=` <option value="${recuperar_cesiones[1].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[1].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[1].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="3" && tiposubdistrito.value==="21") {
                          selectcesiones+=` <option value="${recuperar_cesiones[1].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[1].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[1].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="3" && tiposubdistrito.value==="27") {
                          selectcesiones+=` <option value="${recuperar_cesiones[2].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[2].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[2].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="3" && tiposubdistrito.value==="37") {
                          selectcesiones+=` <option value="${recuperar_cesiones[0].cesion_para_urbanizacion}">Cesion Para Urbanizacion</option>
                                            <option value="${recuperar_cesiones[0].cesion_para_regularizacion}">Cesion Para Regularizacion</option>
                                            <option value="${recuperar_cesiones[0].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="4" && tiposubdistrito.value==="6") {
                          selectcesiones+=` <option value="${recuperar_cesiones[1].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[1].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[1].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="4" && tiposubdistrito.value==="10") {
                          selectcesiones+=` <option value="${recuperar_cesiones[1].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[1].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[1].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="4" && tiposubdistrito.value==="27") {
                          selectcesiones+=` <option value="${recuperar_cesiones[2].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[2].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[2].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "MOLLE" && tipodistrito.value==="4" && tiposubdistrito.value==="28") {
                          selectcesiones+=` <option value="${recuperar_cesiones[2].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[2].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[2].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "ALEJO CALATAYUD" && tipodistrito.value==="5" && tiposubdistrito.value==="12") {
                          selectcesiones+=`<option value="${recuperar_cesiones[0].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "ALEJO CALATAYUD" && tipodistrito.value==="5" && tiposubdistrito.value==="14") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "ALEJO CALATAYUD" && tipodistrito.value==="5" && tiposubdistrito.value==="15") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "ALEJO CALATAYUD" && tipodistrito.value==="5" && tiposubdistrito.value==="16") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "ALEJO CALATAYUD" && tipodistrito.value==="5" && tiposubdistrito.value==="17") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "ALEJO CALATAYUD" && tipodistrito.value==="8" && tiposubdistrito.value==="18") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion</option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "ALEJO CALATAYUD" && tipodistrito.value==="8" && tiposubdistrito.value==="20") {
                          selectcesiones+=` <option value="${recuperar_cesiones[2].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[2].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[2].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        
                        }
                        if (tiposubalcaldia.value === "ALEJO CALATAYUD" && tipodistrito.value==="8" && tiposubdistrito.value==="34") {
                          selectcesiones+=` <option value="${recuperar_cesiones[2].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[2].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[2].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "VALLE HERMOSO" && tipodistrito.value==="6" && tiposubdistrito.value==="16") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        
                        }
                        if (tiposubalcaldia.value === "VALLE HERMOSO" && tipodistrito.value==="7" && tiposubdistrito.value==="16") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        
                        }
                        if (tiposubalcaldia.value === "VALLE HERMOSO" && tipodistrito.value==="7" && tiposubdistrito.value==="19") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        
                        }  
                        if (tiposubalcaldia.value === "VALLE HERMOSO" && tipodistrito.value==="14" && tiposubdistrito.value==="19") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "VALLE HERMOSO" && tipodistrito.value==="14" && tiposubdistrito.value==="20") {
                          selectcesiones+=` <option value="${recuperar_cesiones[2].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[2].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[2].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="9" && tiposubdistrito.value==="14") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="9" && tiposubdistrito.value==="28") {
                          selectcesiones+=` <option value="${recuperar_cesiones[2].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[2].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[2].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
     
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="9" && tiposubdistrito.value==="29") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion</option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="9" && tiposubdistrito.value==="30") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="9" && tiposubdistrito.value==="31") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="9" && tiposubdistrito.value==="32") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="9" && tiposubdistrito.value==="35") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="9" && tiposubdistrito.value==="36") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="15" && tiposubdistrito.value==="32") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="15" && tiposubdistrito.value==="33") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        
                        }
                        if (tiposubalcaldia.value === "ITOCTA" && tipodistrito.value==="15" && tiposubdistrito.value==="35") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="10" && tiposubdistrito.value==="7") {
                          selectcesiones+=`<option value="${recuperar_cesiones[3].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`; 
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="10" && tiposubdistrito.value==="8") {
                          selectcesiones+=`<option value="${recuperar_cesiones[3].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`; 

                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="10" && tiposubdistrito.value==="11") {
                          selectcesiones+=`<option value="${recuperar_cesiones[3].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`; 

                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="10" && tiposubdistrito.value==="12") {
                          selectcesiones+=`<option value="${recuperar_cesiones[3].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones</option>`; 

                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="11" && tiposubdistrito.value==="9") {
                          selectcesiones+=`<option value="${recuperar_cesiones[3].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`; 

                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="11" && tiposubdistrito.value==="13") {
                          selectcesiones+=`<option value="${recuperar_cesiones[3].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`; 
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="11" && tiposubdistrito.value==="16") {
                          selectcesiones+=` <option value="${recuperar_cesiones[4].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[4].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[4].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="12" && tiposubdistrito.value==="2") {
                          selectcesiones+=` <option value="${recuperar_cesiones[0].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[0].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[0].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="12" && tiposubdistrito.value==="3") {
                          selectcesiones+=` <option value="${recuperar_cesiones[0].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[0].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[0].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones</option>`;
                          
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="12" && tiposubdistrito.value==="4") {
                          selectcesiones+=` <option value="${recuperar_cesiones[0].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[0].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[0].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones </option>`;
                        
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="12" && tiposubdistrito.value==="5") {
                          selectcesiones+=` <option value="${recuperar_cesiones[1].cesion_para_urbanizacion}">Cesion Para Urbanizacion</option>
                                            <option value="${recuperar_cesiones[1].cesion_para_regularizacion}">Cesion Para Regularizacion</option>
                                            <option value="${recuperar_cesiones[1].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones</option>`;
                        
                        }
                        if (tiposubalcaldia.value === "ADELA ZAMUDIO" && tipodistrito.value==="12" && tiposubdistrito.value==="6") {
                          selectcesiones+=` <option value="${recuperar_cesiones[1].cesion_para_urbanizacion}">Cesion Para Urbanizacion </option>
                                            <option value="${recuperar_cesiones[1].cesion_para_regularizacion}">Cesion Para Regularizacion </option>
                                            <option value="${recuperar_cesiones[1].excepcion_de_realizar_cesiones}">Excepcion de realizar cesiones</option>`;
                        
                        }





          document.getElementById('tipos_cesiones').innerHTML = selectcesiones
        });         

      }



</script>
{{--Llenado de tablas--}}
<script>
      //poner el valor de los inputs originales en los hidden
  $(function(){
      $(document).on('change','#material_via',function(){ //detectamos el evento change
        var value = $(this).val();//sacamos el valor del select
      // $('#inputhidden10').val(value);//le agregamos el valor al input (notese que el input debe tener un ID para que le caiga el valor)
        $('#tdmaterialvia').val(value);
      });
      $(document).on('change','#Inclinacion',function(){ //detectamos el evento change
        var value = $(this).val();//sacamos el valor del select
      // $('#inputhidden11').val(value);//le agregamos el valor al input (notese que el input debe tener un ID para que le caiga el valor)
        $('#tdfactorinclinacion').val(value);
      });
      $(document).on('change','#ubicacion',function(){ //detectamos el evento change
        var value = $(this).val();//sacamos el valor del select
      // $('#inputhidden12').val(value);//le agregamos el valor al input (notese que el input debe tener un ID para que le caiga el valor)
        $('#tdfactorubicacion').val(value);
      });
      $(document).on('change','#catastral',function(){ //detectamos el evento change
        var value = $(this).val();//sacamos el valor del select
    //   $('#inputhidden9').val(value);//le agregamos el valor al input (notese que el input debe tener un ID para que le caiga el valor)
        $('#tdcatastral').val(value);
      });
      /*$(document).on('change','#tipos_cesiones',function(){ //detectamos el evento change
        var value = $(this).val();//sacamos el valor del select
        $('#tdporcentajecesiones').val(value);
      });*/
      $(document).on('change','#superficieminuta',function(){ //detectamos el evento change
        var value = $(this).val();//sacamos el valor del select
        $('#superficietotalutil').val(value);//le agregamos el valor al input (notese que el input debe tener un ID para que le caiga el valor)
      });
      $(document).on('change','#email_apoderado',function(){ //detectamos el evento change
        var value = $(this).val();//sacamos el valor del select
        $('#inputhidden6').val(value);//le agregamos el valor al input (notese que el input debe tener un ID para que le caiga el valor)
      });
      $(document).on('change','#supescritura',function(){ //detectamos el evento change
        var value = $(this).val();//sacamos el valor del select
        $('#inputhidden7').val(value);//le agregamos el valor al input (notese que el input debe tener un ID para que le caiga el valor)
      });

      $(document).on('change','#cesionefectuada',function(){ //detectamos el evento change
        var convertirendecimal=0;
        var value = $(this).val();//sacamos el valor del select
        convertirendecimal=value / 100;
        //porcentaje del input
        $('#cesionefectuada').val(convertirendecimal);//le agregamos el valor al input (notese que el input debe tener un ID para que le caiga el valor)
        let porcentajedelista= document.getElementById("tipos_cesiones").value;
        var porcentajedecesion=0;
        porcentajedecesion= porcentajedelista-convertirendecimal;
        //resta de los porcentajes
        $('#tdporcentajecesiones').val(porcentajedecesion);//le agregamos el valor al input (notese que el input debe tener un ID para que le caiga el valor)     
        //multiplicacion de d3*h2
        var multiplicaciond3h2=0;
        let superficiesegundocumentolegal= document.getElementById("superficiemensura").value;
        let porcentajecesionencontrada= document.getElementById("tdporcentajecesiones").value;
        multiplicaciond3h2=superficiesegundocumentolegal * porcentajecesionencontrada;
        $('#deficitcesion').val(roundToTwo(multiplicaciond3h2));//le agregamos el valor al input (notese que el input debe tener un ID para que le caiga el valor)     


        
      });
    });
</script>
<script>
  function roundToTwo(num) {
    return +(Math.round(num + "e+2")  + "e-2");
  }
</script>
{{--Llenado de los checkbox--}}
<script >
  $(document).ready(function() {

  $(document).on('click keyup','.mis-checkboxes',function() {
    calcular();
  });

  });

  function calcular() {
    var tot = $('#inputhidden13');
    tot.val(0.20);
    $('.mis-checkboxes').each(function() {
      if($(this).hasClass('mis-checkboxes')) {
        tot.val(($(this).is(':checked') ? parseFloat($(this).attr('tu-attr-precio')) : 0) + parseFloat(tot.val()));  
      }
      else {
        tot.val(parseFloat(tot.val()) + (isNaN(parseFloat($(this).val())) ? 0 : parseFloat($(this).val())));
      }
    });
    var totalParts = parseFloat(tot.val()).toFixed(2).split('.');
    tot.val(totalParts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '.' +  (totalParts.length > 1 ? totalParts[1] : '00'));  
  }
</script>
{{--Function Para El Calculo de los resultados--}}
<script>
  function metodos(){
      let porcentajecesion= document.getElementById("tdporcentajecesiones").value;
      let superficieD3= document.getElementById("superficiemensura").value;
      let superficieD4= document.getElementById("superficieafectada").value;
      //console.log("el porcentaje es " +porcentajecesion+ " la superficie d3 es "+superficieD3+ " la sup d4 es " +superficieD4);
      //Cesion no efectuadas
      var inputcesiones=document.getElementById('inputcesionnoefectuada'); 
      var multiplicaciondeh2d3=porcentajecesion * superficieD3;
      var resultadofinalcesionnoefectuada=multiplicaciondeh2d3-superficieD4;          
      inputcesiones.value=roundToTwo(resultadofinalcesionnoefectuada);
      //Valor Catastral 
      let valorcatastralponderado= document.getElementById("tdcatastral").value;
      let factormaterialvia= document.getElementById("tdmaterialvia").value;
      let fatorinclinacion= document.getElementById("tdfactorinclinacion").value;
      let factorubicacionmanzanoo= document.getElementById("tdfactorubicacion").value;
      let factorservicios= document.getElementById("inputhidden13").value;
      var inputvalorcatastral=document.getElementById('inputcatastralvalor'); 
      var multiplicaciondevalorescatastrales=valorcatastralponderado * factormaterialvia * fatorinclinacion * factorubicacionmanzanoo * factorservicios;
      inputvalorcatastral.value=roundToTwo(multiplicaciondevalorescatastrales);
      //Pago cesiones
      var inputpagototalcesiones=document.getElementById('totalpagocesiones'); 
      var multiplicarparalospagos=multiplicaciondevalorescatastrales * resultadofinalcesionnoefectuada;
      inputpagototalcesiones.value=roundToTwo(multiplicarparalospagos);
      //pago por regularizacion
      
      let tazaderegularizacion= document.getElementById("tasaderegularizacion").value;
      let superficietotalutilinput= document.getElementById("superficietotalutil").value;
      var inputpagoporregularizacion=document.getElementById('pagoporregularizacion'); 
      var multiplicarparalospagosregularizacion= tazaderegularizacion*superficietotalutilinput ;
      inputpagoporregularizacion.value=roundToTwo(multiplicarparalospagosregularizacion);

      //monto liquidacion total
      var inputpagopomontototal=document.getElementById('montototal'); 
      var montototalapagarasiu=multiplicarparalospagosregularizacion +multiplicarparalospagos ;
      inputpagopomontototal.value=roundToTwo(montototalapagarasiu);
  }
</script>