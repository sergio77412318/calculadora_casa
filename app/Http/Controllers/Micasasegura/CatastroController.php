<?php

namespace App\Http\Controllers\Micasasegura;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;


class CatastroController extends Controller
{
    public function getvalorterreno(){
        $datos=DB::table('valor_del_terreno')
        ->select()
        ->get();
        return $datos;
    }
    public function getcoeficientevia(){
        $datos=DB::table('coeficiente_de_via')
        ->select()
        ->get();
        return $datos;
   
    }
    public function getcoeficientetopografico(){
        $datos=DB::table('coeficiente_topografico')
        ->select()
        ->get();
        return $datos;
    }
    public function getcoeficientedeubicacion(){
        $datos=DB::table('coeficiente_de_ubicacion')
        ->select()
        ->get();
        return $datos;
    }
    public function getcoeficientedeservicios(){
        $datos=DB::table('coeficiente_de_servicios')
        ->select()
        ->get();
        return $datos;
    }
    public function getsubalcaldia(){
        $datos=DB::table('subalcaldia')
        ->select()
        ->get();
        return $datos;
    }
    public function getdistrito(){
        $datos=DB::table('distrito')
        ->select()
        ->get();
        return $datos;
    }
    public function getsubdistrito(){
        $datos=DB::table('sub_distrito')
        ->select()
        ->get();
        return $datos;
    }
    public function getzona(){
        $datos=DB::table('zona')
        ->select()
        ->get();
        return $datos;
    }
    public function gettiposdecesiones(){
        $datos=DB::table('tipos_cesiones')
        ->select()
        ->get();
        return $datos;
    }
    

}
