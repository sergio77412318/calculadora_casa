<?php

namespace App\Models\MicasaSegura;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class coeficiente_de_servicios extends Model
{
    use HasFactory;
    
    
    protected $table='coeficiente_de_servicios';
    protected $fillable=[
        'codigo',
        'descripcion_de_servicios',
        'coeficiente',
    ];
}
