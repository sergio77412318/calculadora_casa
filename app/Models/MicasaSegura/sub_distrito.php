<?php

namespace App\Models\MicasaSegura;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sub_distrito extends Model
{
    use HasFactory;
    protected $table='sub_distrito';
    protected $fillable=[
        'numero_sub_distrito',    
    ];
}
