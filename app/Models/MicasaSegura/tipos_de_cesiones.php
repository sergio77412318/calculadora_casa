<?php

namespace App\Models\MicasaSegura;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tipos_de_cesiones extends Model
{
    use HasFactory;
    protected $table='tipos_cesiones';
    protected $fillable=[
        'cesion_para_urbanizacion',
        'cesion_para_regularizacion',
        'excepcion_de_realizar_cesiones',
    ];
}
