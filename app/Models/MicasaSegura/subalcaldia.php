<?php

namespace App\Models\MicasaSegura;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class subalcaldia extends Model
{
    use HasFactory;
    protected $table='subalcaldia';
    protected $fillable=[
        'nombre',    ];
}
