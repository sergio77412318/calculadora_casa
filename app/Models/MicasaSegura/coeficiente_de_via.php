<?php

namespace App\Models\MicasaSegura;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class coeficiente_de_via extends Model
{
    use HasFactory;
    protected $table='coeficiente_de_via';
    protected $fillable=[
        'codigo',
        'material_de_la_via',
        'coeficiente',
    ];
    
            
}
