<?php

namespace App\Models\MicasaSegura;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class zona extends Model
{
    use HasFactory;
    protected $table='zona';
    protected $fillable=[
        'nombre_zona',    
    ];
}
