<?php

namespace App\Models\MicasaSegura;

use Illuminate\Database\Eloquent\Model;

class valor_del_terreno extends Model
{
    use HasFactory;
    protected $table='valor_del_terreno';
    protected $fillable=[
        'zona',
        'valor_catastral_gestion_2017',
    ];
}
