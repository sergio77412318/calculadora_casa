<?php

namespace App\Models\MicasaSegura;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class coeficiente_de_ubicacion extends Model
{
    use HasFactory;
    protected $table='coeficiente_de_ubicacion';
    protected $fillable=[
        'codigo',
        'factor_de_inclinacion',
        'coeficiente',
    ];

    
}
