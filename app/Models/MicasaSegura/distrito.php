<?php

namespace App\Models\MicasaSegura;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class distrito extends Model
{
    use HasFactory;
    protected $table='';
    protected $fillable=[
        'numero_distrito',    
    ];
}
