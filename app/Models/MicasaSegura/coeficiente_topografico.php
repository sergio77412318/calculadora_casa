<?php

namespace App\Models\MicasaSegura;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class coeficiente_topografico extends Model
{
    use HasFactory;
    
    


    protected $table='coeficiente_topografico';
    protected $fillable=[
        'codigo',
        'factor_de_inclinacion',
        'coeficiente',
    ];
}
