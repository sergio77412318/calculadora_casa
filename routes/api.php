<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Micasasegura\CatastroController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/getvalorterreno',[CatastroController::class,'getvalorterreno'])->name('api.getvalorterreno');
Route::get('/getcoeficientevia',[CatastroController::class,'getcoeficientevia'])->name('api.getcoeficientevia');
Route::get('/getcoeficientetopografico',[CatastroController::class,'getcoeficientetopografico'])->name('api.getcoeficientetopografico');
Route::get('/getcoeficientedeubicacion',[CatastroController::class,'getcoeficientedeubicacion'])->name('api.getcoeficientedeubicacion');
Route::get('/getcoeficientedeservicios',[CatastroController::class,'getcoeficientedeservicios'])->name('api.getcoeficientedeservicios');

Route::get('/getsubalcaldia',[CatastroController::class,'getsubalcaldia'])->name('api.getsubalcaldia');
Route::get('/getdistrito',[CatastroController::class,'getdistrito'])->name('api.getdistrito');
Route::get('/getsubdistrito',[CatastroController::class,'getsubdistrito'])->name('api.getsubdistrito');
Route::get('/getzona',[CatastroController::class,'getzona'])->name('api.getzona');
Route::get('/gettiposdecesiones',[CatastroController::class,'gettiposdecesiones'])->name('api.gettiposdecesiones');
