<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('menu');
});

Route::get('/lotesindividualesconpagocesion',function(){
    return view('lotesindividualescpcesion');
})->name('lotesindividualescpcesionmicasasegura');


Route::get('/lote_individual_con_pago_de_cesiones',function(){
    return view('lotecpcesion');
})->name('lotecpcesion');

Route::get('/lote_individual_sin_pago_de_cesiones',function(){
    return view('lotespcesion');
})->name('lotespcesion');


Route::get('/lote_individual_con_planimetria',function(){
    return view('lotecplanimetria');
})->name('lotecplanimetria');


Route::get('/lotes_grupales',function(){
    return view('lotesgrupales');
})->name('lotesgrupales');

