<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MicasaSegura\valor_del_terreno;
class CatastroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $valorterreno = new valor_del_terreno();
        $valorterreno->zona="1";
        $valorterreno->valor_catastral_gestion_2017="1384";
        $valorterreno->created_at=timestamps();
        $valorterreno->updated_at=timestamps();
        $valorterreno->save();
       /* valor_del_terreno::create([
            'zona'	=>	'1',
        	'valor_catastral_gestion_2017'	=>	'1384',
        ]);
        valor_del_terreno::create([
        	
        	'zona'	=>	'2',
        	'valor_catastral_gestion_2017'	=>	'1151',
        ]);
        valor_del_terreno::create([
        	'zona'	=>	'3',
        	'valor_catastral_gestion_2017'	=>	'918',
        ]);
        valor_del_terreno::create([
        	'zona'	=>	'4',
        	'valor_catastral_gestion_2017'	=>	'716',
        ]);
        valor_del_terreno::create([
        	'zona'	=>	'5',
        	'valor_catastral_gestion_2017'	=>	'475',
        ]);
        valor_del_terreno::create([
        	'zona'	=>	'6',
        	'valor_catastral_gestion_2017'	=>	'294',
        ]);
        valor_del_terreno::create([
        	'zona'	=>	'7',
        	'valor_catastral_gestion_2017'	=>	'205',
        ]);
        valor_del_terreno::create([
        	'zona'	=>	'8',
        	'valor_catastral_gestion_2017'	=>	'171',
        ]);
        valor_del_terreno::create([
        	'zona'	=>	'9',
        	'valor_catastral_gestion_2017'	=>	'129',
        ]);
        valor_del_terreno::create([
        	'zona'	=>	'10',
        	'valor_catastral_gestion_2017'	=>	'54',
        ]);
        valor_del_terreno::create([
        	'zona'	=>	'11',
        	'valor_catastral_gestion_2017'	=>	'38',
        ]);          
        coeficiente_de_via::create([
        	'codigo'	=>	'1',
        	'material_de_la_via'	=>	'Asfalto, Pavimento Rigido',
            'coeficiente'	=>	'1.00',
        ]);
        coeficiente_de_via::create([
        	'codigo'	=>	'2',
        	'material_de_la_via'	=>	'Adoquin',
            'coeficiente'	=>	'0.84',
        ]);
        coeficiente_de_via::create([
        	'codigo'	=>	'3',
        	'material_de_la_via'	=>	'Cemento',
            'coeficiente'	=>	'0.85',
        ]);
        coeficiente_de_via::create([
        	'codigo'	=>	'4',
        	'material_de_la_via'	=>	'Loseta',
            'coeficiente'	=>	'0.84',
        ]);
        coeficiente_de_via::create([
        	'codigo'	=>	'5',
        	'material_de_la_via'	=>	'Empedrado',
            'coeficiente'	=>	'0.65',
        ]);
        coeficiente_de_via::create([
        	'codigo'	=>	'6',
        	'material_de_la_via'	=>	'Ripiado',
            'coeficiente'	=>	'0.61',
        ]);
        coeficiente_de_via::create([
        	'codigo'	=>	'7',
        	'material_de_la_via'	=>	'Tierra',
            'coeficiente'	=>	'0.57',
        ]);
        coeficiente_de_via::create([
        	'codigo'	=>	'8',
        	'material_de_la_via'	=>	'Otros',
            'coeficiente'	=>	'0.57',
        ]);
        coeficiente_topografico::create([
        	'codigo'	=>	'1',
        	'factor_de_inclinacion'	=>	'Inclinacion de 0 a 10 grados',
            'coeficiente'	=>	'1.00',
        ]);
        coeficiente_topografico::create([
        	'codigo'	=>	'2',
        	'factor_de_inclinacion'	=>	'Inclinacion de 11 a 15 grados',
            'coeficiente'	=>	'0.90',
        ]);
        coeficiente_topografico::create([
        	'codigo'	=>	'3',
        	'factor_de_inclinacion'	=>	'Inclinacion Superior a 15 grados',
            'coeficiente'	=>	'0.80',
        ]);
        coeficiente_de_ubicacion::create([
        	'codigo'	=>	'1',
        	'factor_de_inclinacion'	=>	'Esquina',
            'coeficiente'	=>	'1.00',
        ]);
        coeficiente_de_ubicacion::create([
        	'codigo'	=>	'2',
        	'factor_de_inclinacion'	=>	'Medio',
            'coeficiente'	=>	'1.00',
        ]);
        coeficiente_de_ubicacion::create([
        	'codigo'	=>	'3',
        	'factor_de_inclinacion'	=>	'Pasaje Interior',
            'coeficiente'	=>	'0.80',
        ]);
        coeficiente_de_ubicacion::create([
        	'codigo'	=>	'4',
        	'factor_de_inclinacion'	=>	'No Definido',
            'coeficiente'	=>	'1.00',
        ]);
        coeficiente_de_servicios::create([
        	'codigo'	=>	'1',
        	'descripcion_de_servicios'	=>	'Agua Potable',
            'coeficiente'	=>	'0.20',
        ]);
        coeficiente_de_servicios::create([
        	'codigo'	=>	'2',
        	'descripcion_de_servicios'	=>	'Alcantarillado',
            'coeficiente'	=>	'0.20',
        ]);
        coeficiente_de_servicios::create([
        	'codigo'	=>	'3',
        	'descripcion_de_servicios'	=>	'Energia Electrica',
            'coeficiente'	=>	'0.20',
        ]);
        coeficiente_de_servicios::create([
        	'codigo'	=>	'4',
        	'descripcion_de_servicios'	=>	'Telefono',
            'coeficiente'	=>	'0.20',
        ]);*/
    }
}
