<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoeficienteDeServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coeficiente_de_servicios', function (Blueprint $table) {
            $table->id();
            $table->text('codigo')->nullable();
            $table->text('descripcion_de_servicios')->nullable();
            $table->text('coeficiente')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coeficiente_de_servicios');
    }
}
