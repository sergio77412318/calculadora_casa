<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTiposCesionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_cesiones', function (Blueprint $table) {
            $table->id();
            $table->text('cesion_para_urbanizacion')->nullable();
            $table->text('cesion_para_regularizacion')->nullable();
            $table->text('excepcion_de_realizar_cesiones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos_cesiones');
    }
}
